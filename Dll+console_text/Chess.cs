﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_kur
{
    public class Chess
    {
        public string fen { get; set; }
        Board board;
        Moves moves;
        List<FigureMoving> allMoves;
        //rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
        //public Chess(string fen = "1k6/8/8/8/Pp6/8/8/6K1 b - - 0 1")
        //rnbqkbnr/8/8/8/8/8/8/RNBQKBNR w KQkq - 0 1
        public Chess(string fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
        {
            this.fen = fen;
            board = new Board(fen);
            moves = new Moves(board);

        }

        Chess(Board board)
        {
            this.board = board;
            this.fen = board.fen;
            moves = new Moves(board);
        }
        //работает
        public Chess Move(string move)
        {
            FigureMoving fm = new FigureMoving(move);
            if (!moves.CanMove(fm))
                return this;
            if (board.IsCheckAfterMove(fm))
                return this;
            Board nextBoard = board.Move(fm);
            Chess nextChess = new Chess(nextBoard);
            //Console.WriteLine("a3 " + GetFigureAt(0,2));
            return nextChess;
        }

        public char GetFigureAt(int x, int y)
        {
            Coord coord = new Coord(x, y);
            Figure f = board.GetFigureAt(coord);
            return f == Figure.none ? '.' : (char)f;
        }
        
        void FindAllMoves()
        {
            allMoves = new List<FigureMoving>();
            foreach (FigureOnCoord fs in board.YieldFigures())
            {
                foreach (Coord to in Coord.YieldCoord())
                {
                    FigureMoving fm = new FigureMoving(fs, to);
                    if (moves.CanMove(fm))
                        if (!board.IsCheckAfterMove(fm))
                            allMoves.Add(fm);
                    
                }
            }
            /*
            string aa = "-";
            if (board.previousturn != null)
            {
                aa = board.previousturn.ToString();
                Console.WriteLine("Previous turn: " + aa);
                Console.WriteLine("Клетка " + aa.Substring(1, 2));
            }
            */
            /*
            if (board.previousturn!=null)
                Console.WriteLine("Previous turn from: x " + board.previousturn.from.x + " y " + board.previousturn.from.y);
            if (board.previousturn!= null)
                Console.WriteLine("Previous turn to: x " + board.previousturn.to.x + " y " + board.previousturn.to.y);
                */
            /*
            Console.WriteLine("Black king moved?");
            Console.WriteLine(board.bkm);
            Console.WriteLine("White king moved?");
            Console.WriteLine(board.wkm);
            Console.WriteLine("Black short rook moved?");
            Console.WriteLine(board.bsrm);
            Console.WriteLine("White short rook moved?");
            Console.WriteLine(board.wsrm);
            Console.WriteLine("Black long rook moved?");
            Console.WriteLine(board.blrm);
            Console.WriteLine("White long rook moved?");
            Console.WriteLine(board.wlrm +"\n");
            */
        }
        public List<string> GetAllMoves()
        {
            FindAllMoves();
            List<string> list = new List<string>();
            foreach (FigureMoving fm in allMoves)
                list.Add(fm.ToString());
            return list;
        }

        public bool IsCheck()
        {
            return board.IsCheck();
        }

        public bool IsMate()
        {
            bool ZeroMoves = false;
            FindAllMoves();
            if (allMoves.Count == 0)
            {
                ZeroMoves = true;
            }
            return IsCheck() && ZeroMoves;
        }

        public bool IsStalemate()
        {
            bool ZeroMoves = false;
            FindAllMoves();
            if (allMoves.Count == 0)
            {
                ZeroMoves = true;
            }
            if (IsCheck() == false && ZeroMoves == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        ///если количество ходов 0 и при этом шах, значит это мат
        ///list(t).Count - количество элементов в списке
        ///если количество ходов 0, а шаха нет значит это пат
    }
}
