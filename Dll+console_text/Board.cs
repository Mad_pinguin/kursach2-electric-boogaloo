﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_kur
{
    class Board
    {
        public string fen { get; private set; }
        Figure[,] figures;
        public Color moveColor { get; private set; }
        public int moveNumber { get; private set; }
        public bool bkm = false; //черный король
        public bool wkm = false;
        public bool wlrm = false; //белая дальняя ладья
        public bool blrm = false;
        public bool wsrm = false; //белая ближняя ладья  = false
        public bool bsrm = false;
        public FigureMoving previousturn;
        public bool previousturnjump = false;
        //Coord jumpedcoord;
        public Board(string fen)
        {
            this.fen = fen;
            figures = new Figure[8, 8];
            Init();
        }
        void Init()
        {
            //"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR" "w                " "KQkq                                        " "-                                       " "0                               " "1                "
            //"расположение фигур на доске                " "чей следующий ход" "K/k короткая рокировка Q/q длинная рокировка" "клетка, через которую перепрыгнула пешка" "число ходов для правила 50 ходов" "номер хода сейчас"                          
            string[] parts = fen.Split();
            if (parts.Length != 6) return;
            InitFigures(parts[0]);
            moveColor = (parts[1] == "b") ? Color.black : Color.white;
            //StringBuilder castle = new StringBuilder(parts[2]);
            if (parts[2].Contains('K') == false) //белые короткая
            {
                wsrm = true;
               // CanSCW = false;
            }
            if (parts[2].Contains('Q') == false) //белые длинная
            {
                wlrm = true;
                //CanSCB = false;
            }
            if (parts[2].Contains('k') == false) //чёрные короткая
            {
                bsrm = true;
                //CanLCW = false;
            }
            if (parts[2].Contains('q') == false) //чёрные длинная
            {
                blrm = true;
                //CanLCB = false;
            }
            if (parts[2].Contains('-') == true) //никакая
            {
                wkm = true;
                bkm = true;
                /*
                CanSCW = false;
                CanSCB = false;
                CanLCW = false;
                CanLCB = false;
                */
            }
            if (parts[3] != "-")
            {
                previousturn = getprevturn(fen);
                previousturnjump = true;
            }
            moveNumber = int.Parse(parts[5]);
        }
        FigureMoving getprevturn(string fen)
        {
            string[] parts = fen.Split();
            string hod = "";
            
                //Console.WriteLine(parts[3]);
                previousturnjump = true;
                int stepY = moveColor == Color.white ? 1 : -1;
                if (moveColor == Color.white)
                {
                    hod += "p";
                }
                else
                {
                    hod += "P";
                }
                hod += parts[3].Substring(0, 1);
                hod += Convert.ToInt32(parts[3].Substring(1, 1)) + stepY;
                hod += parts[3].Substring(0, 1);
                hod += Convert.ToInt32(parts[3].Substring(1, 1)) - stepY;
                //Console.WriteLine(hod);
            //return hod;
            FigureMoving fm = new FigureMoving(hod);
            return fm;
        }
        /*
        bool CanSCW;
        bool CanSCB;
        bool CanLCW;
        bool CanLCB;
        */
        bool ShortCastlingWhite()
        {
            if (wkm == true || wsrm == true)
            {
                return false;
            }
            return true;
        }
        bool LongCastlingWhite()
        {
            if (wkm == true || wlrm == true)
            {
                return false;
            }
            return true;
        }
        bool ShortCastlingBlack()
        {
            if (bkm == true || bsrm == true)
            {
                return false;
            }
            return true;
        }
        bool LongCastlingBlack()
        {
            if (bkm == true || blrm == true)
            {
                return false;
            }
            return true;
        }
        void InitFigures(string data)
        {
            for (int j = 8; j >= 2; j--)
            {
                data = data.Replace(j.ToString(), (j - 1).ToString() + "1");
            }
            data = data.Replace("1", ".");
            string[] lines = data.Split('/');
            for (int y = 7; y >= 0; y--)
                for (int x = 0; x < 8; x++)
                    figures[x, y] = lines[7-y][x]== '.' ? Figure.none :
                        (Figure)lines[7 - y][x];
        }

        public IEnumerable<FigureOnCoord> YieldFigures()
        {
            
            foreach (Coord coord in Coord.YieldCoord())
            {
                Figure test = GetFigureAt(coord);
               // Console.Write(test +"\n");
                if (test.GetColor() == moveColor)
                {
                 //   Console.Write("координата -"+ coord.Name + "\n");
             //       Console.Write(coord.Name);
                    yield return new FigureOnCoord(test, coord);
                }

            }
        }

        bool CanEatKing()
        {
            Coord badKing = FindBadKing();
            Moves moves = new Moves(this);
            foreach (FigureOnCoord fs in YieldFigures())
            {
                FigureMoving fm = new FigureMoving(fs, badKing);
                if (moves.CanMove(fm))
                    return true;
            }
            return false;
        }

        private Coord FindBadKing()
        {
            //Console.WriteLine(moveColor);
            Figure badKing = moveColor == Color.black ? Figure.whiteKing : Figure.blackKing;
            foreach (Coord coord in Coord.YieldCoord())
            {
                if (GetFigureAt(coord) == badKing)
                {
                    return coord;
                }
            }
            return Coord.none;
        }

        public bool IsCheck()
        {
            Board after = new Board(fen);
            after.moveColor = moveColor.FlipColor();
            return after.CanEatKing();
        }

        public Figure GetFigureAt(Coord coord)
        {
            if (coord.OnBoard())
            {
                return figures[coord.x, coord.y];
            }
            return Figure.none;
        }
        /*
        void SetFigureAt(Coord coord, Figure figure)
        {
            if (coord.OnBoard())
            {
                figures[coord.x, coord.y] = figure;
                if (figure == Figure.whiteKing)
                {
                    wkm = true;
                }
                if (figure == Figure.blackKing)
                {
                    bkm = true;
                }
                
                if (figure == Figure.whiteRook)
                {
                    if (coord.x == 0 && coord.y == 0)
                    {
                        wlrm = true;
                    }
                }
                if (figure == Figure.whiteRook)
                {
                    if (coord.x == 7 && coord.y == 0)
                    {
                        wsrm = true;
                    }
                }
                if (figure == Figure.blackRook)
                {
                    if (coord.x == 0 && coord.y == 7)
                    {
                        blrm = true;
                    }
                }
                if (figure == Figure.blackRook)
                {
                    if (coord.x == 7 && coord.y == 7)
                    {
                        bsrm = true;
                    }
                }
            }
        }
        void SetRookAt(Coord from, Coord to, Figure figure)
        {
            if (to.OnBoard())
            {
                figures[from.x, from.y] = Figure.none;
                figures[to.x, to.y] = figure;
                if (figure == Figure.whiteRook)
                {
                    if (from.x == 0 && from.y == 0)
                    {
                        wlrm = true;
                    }
                    
                }
            }
        }
        */

        //шах на конкретной клетке
        public bool CanEatKingAt(Coord to)
        {
            Moves moves = new Moves(this);
            foreach (FigureOnCoord fs in YieldFigures())
            {
                FigureMoving fm = new FigureMoving(fs, to);
                if (moves.CanMove(fm))
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsCheckAt(Coord to)
        {
            Board after = new Board(fen);
            after.moveColor = moveColor.FlipColor();
            return after.CanEatKingAt(to);
        }
        //шах на конкретной клетке
        void SetFigureAt(Coord from, Coord to, Figure figure)
        {
            if (from.OnBoard())
            {
                if (to.OnBoard())
                {
                    figures[from.x, from.y] = Figure.none;
                    figures[to.x, to.y] = figure;
                    if (figure == Figure.whiteKing)
                    {
                        wkm = true;
                    }
                    if (figure == Figure.blackKing)
                    {
                        bkm = true;
                    }
                    if (figure == Figure.whiteRook)
                    {
                        if (from.x == 0 && from.y == 0)
                        {
                            wlrm = true;
                        }
                    }
                    if (figure == Figure.blackRook)
                    {
                        if (from.x == 0 && from.y == 7)
                        {
                            blrm = true;
                        }
                    }
                    if (figure == Figure.whiteRook)
                    {
                        if (from.x == 7 && from.y == 0)
                        {
                            wsrm = true;
                        }
                    }
                    if (figure == Figure.blackRook)
                    {
                        if (from.x == 7 && from.y == 7)
                        {
                            bsrm = true;
                        }
                    }
                }
            }
        }

        string FenFigures()
        {
            StringBuilder sb = new StringBuilder();
            for (int y = 7; y >= 0; y--)
            {
                for (int x = 0; x < 8; x++)
                    sb.Append(figures[x, y] == Figure.none ? '1' : (char)figures[x, y]);
                if (y>0)
                    sb.Append('/');
            }
            string eight = "11111111";
            for (int j = 8; j>=2; j--)
                sb.Replace(eight.Substring(0,j), j.ToString());
            return sb.ToString();
        }

        void GetFen()
        {
            this.fen = FenFigures() + " " +
                   (moveColor == Color.white ? "w" : "b") + " " +
                   //" -" + 
                   FenCastling() + " " +
                   FenEnPassant() +
                   " 0 " + moveNumber.ToString();
        }

        public string FenEnPassant()
        {
            if (previousturn != null)
            {
                if (previousturn.figure == Figure.blackPawn || previousturn.figure == Figure.whitePawn)
                {
                    if (previousturnjump == true)
                    {
                        int stepY = previousturn.figure.GetColor() == Color.white ? 1 : -1;
                        Coord jumpedcoord = new Coord(previousturn.from.x, previousturn.from.y + stepY);
                        string en_pas_str;
                        //en_pas_str = previousturn.ToString().Substring(1) + jumpedcoord.y.ToString();
                        //en_pas_str = jumpedcoord.ToString();
                        en_pas_str = previousturn.ToString().Substring(1,1) + (jumpedcoord.y+1).ToString();
                        return en_pas_str;
                    }
                }
            }
            return "-";
        }

        public Board Move(FigureMoving fm)
        {
            Board next = new Board(fen);
            next.wkm = wkm;
            next.bkm = bkm;
            next.wsrm = wsrm;
            next.wlrm = wlrm;
            next.bsrm = bsrm;
            next.blrm = blrm;
            /*
            next.SetFigureAt(fm.from, Figure.none);
            next.SetFigureAt(fm.to, fm.promotion == Figure.none ? fm.figure : fm.promotion);
            */
            
            ///White KING long
            if (fm.from.x == 4 && fm.from.y == 0 && fm.to.x == 2 && fm.to.y == 0)
            {
                next.SetFigureAt(fm.from, fm.to, fm.promotion == Figure.none ? fm.figure : fm.promotion);
                Coord from = new Coord(0, 0);
                Coord to = new Coord(3, 0);
                Figure rook = Figure.whiteRook;
                next.SetFigureAt(from, to, rook);
            }
            //Black king long
            if (fm.from.x == 4 && fm.from.y == 7 && fm.to.x == 2 && fm.to.y == 7)
            {
                next.SetFigureAt(fm.from, fm.to, fm.promotion == Figure.none ? fm.figure : fm.promotion);
                Coord from = new Coord(0, 7);
                Coord to = new Coord(3, 7);
                Figure rook = Figure.blackRook;
                next.SetFigureAt(from, to, rook);
            }
            //white king short
            if (fm.from.x == 4 && fm.from.y == 0 && fm.to.x == 6 && fm.to.y == 0)
            {
                next.SetFigureAt(fm.from, fm.to, fm.promotion == Figure.none ? fm.figure : fm.promotion);
                Coord from = new Coord(7, 0);
                Coord to = new Coord(5, 0);
                Figure rook = Figure.whiteRook;
                next.SetFigureAt(from, to, rook);
            }
            //black king short
            if (fm.from.x == 4 && fm.from.y == 7 && fm.to.x == 6 && fm.to.y == 7)
            {
                next.SetFigureAt(fm.from, fm.to, fm.promotion == Figure.none ? fm.figure : fm.promotion);
                Coord from = new Coord(7, 7);
                Coord to = new Coord(5, 7);
                Figure rook = Figure.blackRook;
                next.SetFigureAt(from, to, rook);
            }
            next.previousturnjump = false;
            next.SetFigureAt(fm.from, fm.to, fm.promotion == Figure.none ? fm.figure : fm.promotion);
            if (moveColor == Color.black)
            {
                next.moveNumber++;
            }
            if (previousturn != null)
            {
                if (previousturn.figure == Figure.blackPawn || previousturn.figure == Figure.whitePawn)
                {
                    if (previousturnjump == true)
                    {
                        int stepY = previousturn.figure.GetColor() == Color.white ? 1 : -1;
                        Coord jumpedcoord = new Coord(previousturn.from.x, previousturn.from.y + stepY);
                        if (next.GetFigureAt(jumpedcoord) == Figure.whitePawn || next.GetFigureAt(jumpedcoord) == Figure.blackPawn)
                        {
                            next.SetFigureAt(previousturn.to, previousturn.to, Figure.none);
                        }
                    }
                }
            }
            next.moveColor = moveColor.FlipColor();
            if (next.moveColor == Color.black) //следующими ходят чёрные // int stepY = fm.figure.GetColor() == Color.white ? 1 : -1;
            {
                if (fm.figure == Figure.whitePawn)
                {
                    if (fm.AbsDeltaY == 2)
                    {
                        next.previousturnjump = true;
                    }
                }
            }
            else
            {
                if (fm.figure == Figure.blackPawn)
                {
                    if (fm.AbsDeltaY == 2)
                    {
                        next.previousturnjump = true;
                    }
                }
            }
            next.previousturn = fm;
            next.GetFen();
            return next;
        }
        string FenCastling()
        {
            string CastFen = "";
            if (ShortCastlingWhite() == true)
            {
                CastFen += "K";
            }
            if (LongCastlingWhite() == true)
            {
                CastFen += "Q";
            }
            if (ShortCastlingBlack() == true)
            {
                CastFen += "k";
            }
            if (LongCastlingBlack() == true)
            {
                CastFen += "q";
            }
            if (ShortCastlingWhite() == false && LongCastlingWhite() == false && ShortCastlingBlack() == false && LongCastlingBlack() == false)
            {
                CastFen += "-";
            }
            return CastFen;
        }

        public bool IsCheckAfterMove(FigureMoving fm)
        {
            Board after = Move(fm);
            return after.CanEatKing();
        }

    }
}
