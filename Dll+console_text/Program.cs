﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess_kur;

namespace DemoChess
{
    class Program
    {
        static void Main(string[] args)
        {
            //Тест регресии
            /*test_moves();
            Console.ReadLine();*/
            
            Chess chess = new Chess();
            while (true)
            {
                Console.WriteLine(chess.fen);
                Console.WriteLine(ChessToAscii(chess));
                if (chess.IsStalemate() == true)
                {
                    Console.WriteLine("Stalemate!\nGAME OVER");
                    Console.ReadLine();
                    break;
                }
                if (chess.IsCheck() == true)
                {
                    if (chess.IsMate() == true)
                    {
                        if (chess.fen.Split()[1] == "w")
                        {
                            Console.WriteLine("Победили чёрные");
                        }
                        else
                        {
                            Console.WriteLine("Победили белые");
                        }
                        Console.WriteLine("MATE!\nGAME OVER");
                        Console.ReadLine();
                        break;
                    }
                    Console.WriteLine("CHECK");
                }
                
                foreach (string moves in chess.GetAllMoves())
                    Console.Write(moves + "\t");
                Console.WriteLine();
                Console.Write("> ");
                string move = Console.ReadLine();
                if (move == "q")
                {
                    break;
                }

                chess = chess.Move(move);
            }
        }

        static string ChessToAscii(Chess_kur.Chess Chess)
        {
            string text = "  +-----------------+\n";
            for (int y = 7; y >= 0; y--)
            {
                text += y + 1;
                text += " | ";
                for (int x = 0; x < 8; x++)
                {
                    text += Chess.GetFigureAt(x, y) + " ";
                }
                text += "|\n";
            }
            text += "  +-----------------+\n";
            text += "    a b c d e f g h\n";
            return text;
        }

        static void test_moves()
        {
            Chess chess = new Chess();
            List<string> listOfMoves = new List<string>()
            {
                "Pf2f4",
                "pe7e6",
                "Pg2g3",
                "bf8b4",
                "Bf1h3",
                "ng8f6",
                "Ng1f3",
                "ke8g8",
                "ke8g8",
                "Ke1g1",
                "nf6g4",
                "Bh3g4",
                "pc7c5",
                "Pf4f5",
                "pc5c4",
                "Pd2d4",
                "pc4d3",
                "Pf5e6",
                "pd3e2",
                "Nf3d4",
                "pe2d1q",
                "Nb1c3",
                "qd8f6",
                "Pa2a3",
                "bb4c5",
                "Pa3a4",
                "bc5d4",
                "Kg1g2",
                "qd1f1",
                "pa1a9"
            };
            foreach (string move in listOfMoves)
            {
                Console.WriteLine(chess.fen);
                Console.WriteLine(ChessToAscii(chess));
                if (chess.IsStalemate() == true)
                {
                    Console.WriteLine("Stalemate!\nGAME OVER");
                    Console.ReadLine();
                    break;
                }
                if (chess.IsCheck() == true)
                {
                    if (chess.IsMate() == true)
                    {
                        if (chess.fen.Split()[1] == "w")
                        {
                            Console.WriteLine("Победили чёрные");
                        }
                        else
                        {
                            Console.WriteLine("Победили белые");
                        }
                        Console.WriteLine("MATE!\nGAME OVER");
                        Console.ReadLine();
                        break;
                    }
                    Console.WriteLine("CHECK");
                }

                foreach (string moves in chess.GetAllMoves())
                    Console.Write(moves + "\t");
                Console.WriteLine();
                Console.Write("> ");
                if (move == "q")
                {
                    break;
                }

                chess = chess.Move(move);
            }
        }
    }
}
