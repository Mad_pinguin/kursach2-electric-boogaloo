﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_kur
{
    class Moves
    {
        FigureMoving fm;
        Board board;
        public Moves(Board board)
        {
            this.board = board;
        }
        //вызываемая функция
        public bool CanMove(FigureMoving fm)
        {
            this.fm = fm;
            return
                CanMoveFrom() &&
                CanMoveTo() &&
                CanFigureMove();
        }
        bool CanMoveFrom()
        {
            return fm.from.OnBoard() &&
                       board.GetFigureAt(new Coord(fm.from.x, fm.from.y)) == fm.figure &&
                       fm.figure.GetColor() == board.moveColor;
        }
        
        bool CanMoveTo()
        {
            return fm.to.OnBoard() &&
                fm.from != fm.to &&
                board.GetFigureAt(fm.to).GetColor() != board.moveColor;
        }

        bool CanFigureMove()
        {
            switch (fm.figure)
            {
                case Figure.whiteKing:
                case Figure.blackKing:
                    return CanKingMove();

                case Figure.whiteQueen:
                case Figure.blackQueen:
                    return CanStraightMove();

                case Figure.whiteRook:
                case Figure.blackRook:
                    return (fm.SignX==0||fm.SignY==0) &&
                            CanStraightMove();

                case Figure.whiteBishop:
                case Figure.blackBishop:
                    return (fm.SignX != 0 && fm.SignY != 0) &&
                            CanStraightMove();

                case Figure.whiteKnight:
                case Figure.blackKnight:
                    return CanKnightMove();

                case Figure.whitePawn:
                case Figure.blackPawn:
                    return CanPawnMove();
                default: return false;
            }
        }
        /*
        private bool CanPawnMove()
        {
            if (fm.from.y < 1 || fm.from.y > 6)
                return false;
            int stepY = fm.figure.GetColor() == Color.white ? 1 : -1;
            return
                CanPawnGo(stepY) ||
                CanPawnJump(stepY) ||
                CanPawnEat(stepY);

        }*/
        
        private bool CanPawnMove()
        {
            if (fm.from.y < 1 || fm.from.y > 6)
                return false;
            int stepY = fm.figure.GetColor() == Color.white ? 1 : -1;
            return
                CanPawnGo(stepY) ||
                CanPawnJump(stepY) ||
                CanPawnEat(stepY) ||
                CanPawnEatTricky(stepY);
        }
        
            //ещё одна функция для взятия на проходе

        private bool CanPawnGo(int stepY)
        {
            if (board.GetFigureAt(fm.to) == Figure.none)
                    if (fm.DeltaX == 0)
                        if (fm.DeltaY == stepY)
                            return true;
            return false;
        }

        private bool CanPawnJump(int stepY)
        {
            if (board.GetFigureAt(fm.to) == Figure.none)
                if (fm.DeltaX == 0)
                    if (fm.DeltaY == 2 * stepY)
                        if (fm.from.y == 1 || fm.from.y == 6)
                            if (board.GetFigureAt(new Coord(fm.from.x, fm.from.y + stepY)) == Figure.none)
                                return true;
            return false;
        }

        private bool CanPawnEat(int stepY)
        {
            if (board.GetFigureAt(fm.to) != Figure.none)
                if (fm.AbsDeltaX == 1)
                    if (fm.DeltaY == stepY)
                        return true;
            return false;
        }

        //взятие на проходе
        private bool CanPawnEatTricky(int stepY)
        {
            Coord testing = new Coord(fm.to.x, fm.to.y-stepY);
            if (board.moveColor == Color.black)
            {
                if (board.GetFigureAt(testing) == Figure.whitePawn)
                    if (fm.AbsDeltaX == 1)
                        if (fm.DeltaY == stepY)
                            if (board.previousturnjump == true)
                                if (board.previousturn.to == testing)
                                    return true;
                return false;
            }
            else
            {
                if (board.GetFigureAt(testing) == Figure.blackPawn)
                    if (fm.AbsDeltaX == 1)
                        if (fm.DeltaY == stepY)
                            if (board.previousturnjump == true)
                                if (board.previousturn.to == testing)
                                    return true;
                return false;
            }

        }

        private bool CanKingMove()
        {
            if (fm.AbsDeltaX <= 1 && fm.AbsDeltaY <= 1)
            {
                return true;
            }
            if (fm.DeltaX == -2 && fm.AbsDeltaY == 0)
            {
                return CanLongCastle();
            }
            if (fm.DeltaX == 2 && fm.AbsDeltaY == 0)
            {
                return CanShortCastle();
            }
            return false;
        }
        
        private bool CanLongCastle()
        {
            if (fm.figure == Figure.blackKing && board.bkm == false)
            {
                if (board.blrm == false)
                {
                    if (CanStraightMoveKing() == true)
                    {
                        if (NoFigCastleCheck() == true)
                        {
                            return true;
                        }
                    }
                }
            }
            if (fm.figure == Figure.whiteKing && board.wkm == false)
            {
                if (board.wlrm == false)
                {
                    if (CanStraightMoveKing() == true)
                    {
                        if (NoFigCastleCheck() == true)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        private bool CanShortCastle()
        {
            if (fm.figure == Figure.blackKing && board.bkm == false)
            {
                if (board.bsrm == false)
                {
                    if (CanStraightMoveKing() == true)
                    {
                        if (NoFigCastleCheck() == true)
                        {
                            return true;
                        }
                    }
                }
            }
            if (fm.figure == Figure.whiteKing && board.wkm == false)
            {
                if (board.wsrm == false)
                {
                    if (CanStraightMoveKing() == true)
                    {
                        if (NoFigCastleCheck() == true)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        
        private bool CanStraightMove()
        {
            Coord at = fm.from;
            do
            {
                at = new Coord(at.x + fm.SignX, at.y + fm.SignY);
                if (at == fm.to)
                    return true;
            } while (at.OnBoard() && board.GetFigureAt(at) == Figure.none);
            return false;
        }
        private bool CanStraightMoveKing()
        {
            if (board.IsCheck() == true)
            {
                return false;
            }
            Coord at = fm.from;
            do
            {
                at = new Coord(at.x + fm.SignX, at.y);
                if (at == fm.to)
                    return true;
            } while (at.OnBoard() && board.GetFigureAt(at) == Figure.none && board.IsCheckAt(at) == false);
            
            return false;
        }

        private bool NoFigCastleCheck()
        {
            Coord b = fm.to;
            b = new Coord(b.x - 1, b.y);
            Coord at = fm.from;
            do
            {
                at = new Coord(at.x + fm.SignX, at.y + fm.SignY);
                if (at == b)
                    return true;
            } while (at.OnBoard() && board.GetFigureAt(at) == Figure.none);
            return false;
        }

        private bool CanKnightMove()
        {
            if (fm.AbsDeltaX == 1 && fm.AbsDeltaY == 2) return true;
            if (fm.AbsDeltaX == 2 && fm.AbsDeltaY == 1) return true;
            return false;
        }
    }
}
