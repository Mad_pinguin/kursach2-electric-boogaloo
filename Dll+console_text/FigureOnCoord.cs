﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_kur
{
    class FigureOnCoord
    {
        public Figure figure { get; private set; }
        public Coord coord { get; private set; }

        public FigureOnCoord(Figure figure, Coord coord)
        {
            this.figure = figure;
            this.coord = coord;
        }
    }
}
