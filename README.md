# Шахматы с рейтингом

Выполнил студент группы 728-2 Давыденко Сергей

## Краткое описание программы

Данная программа является клиент-серверным приложением, позволяющим пользователю играть в шахматы с другим игроком, а также учитывать рейтинг.

## Правила игры

В данной программе реализовано большинство правил игры, в том числе ходы фигур, рокировка, взятие на проходе, превращение пешки.
С полным перечнем правил игры можно ознакомиться по следующей [ссылке](https://ru.wikipedia.org/wiki/Правила_шахмат), но не все правила из неё были реализованы (например не было реализовано правило 50 ходов)

## Предварительная подготовка к работе

Существует возможность скомпилировать программу из исходного кода через Unity, или можно воспользоваться заранее собранным приложением. Также рекомендуется воспользоваться программой Hamachi для соединения по сети (файл установщик находится в папке Hamachi).

## Управление

Управление осуществляется клавиатурой и мышью, для передвижения фигур необходимо зажать левую кнопку мыши на фигуре, которую необходимо переместить и затем перетащить её в желаемое место.
