﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using System.Text;
using System.IO;
using System.Security.Cryptography;

public class User
{

    private string _login, _password;
    public User(string login, string password)
    {

        _login = login;
        _password = MakeHash.Hash(password);
    }
    DataBase MakeHash = new DataBase();
    private SqliteConnection UsersDB;
    public bool CheckUser()
    {
        MakeHash.DBCheck();
        UsersDB = new SqliteConnection("Data Source = DB.db ; Version=3");
        UsersDB.Open();
        SqliteCommand CMD = UsersDB.CreateCommand();
        CMD.CommandText = "select Name from Users where Name like @Log and Password like @Pas";
        CMD.Parameters.Add("@Log", System.Data.DbType.String).Value = _login;
        CMD.Parameters.Add("@Pas", System.Data.DbType.String).Value = _password;
        SqliteDataReader SQL = CMD.ExecuteReader();
        if (SQL.HasRows)
        {
            while (SQL.Read())
            {
                return true;
            }
            return true;
        }
        else
        {
            _login = string.Empty;
            _password = string.Empty;
            return false;
        }
    }
}
