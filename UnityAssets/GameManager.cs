﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using Mono.Data.Sqlite;
using System.Data;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    DataBase test = new DataBase();
    public static GameManager Instace { set; get; }

    public GameObject mainMenu;

    public GameObject serverMenu;
    public GameObject connectMenu;
    public GameObject Login;
    public GameObject Registration;
    public GameObject serverPrefab;
    public GameObject clientPrefab;

    //public InputField nameInput;
    public InputField loginInput;
    public InputField passwordInput;

    public InputField loginInput_r;
    public InputField passwordInput_r;

    public Text Rating;

    private void Start()
    {
        Instace = this;
        test.DBCheck();
        Registration.SetActive(false);
        connectMenu.SetActive(false);
        serverMenu.SetActive(false);
        Login.SetActive(true);
        mainMenu.SetActive(false);
        DontDestroyOnLoad(gameObject);
    }


    public string PlayerName;
    public void ConnectButton()
    {
        mainMenu.SetActive(false);
        connectMenu.SetActive(true);
    }

    public void HostButton()
    {
        try
        {
            Client c = Instantiate(clientPrefab).GetComponent<Client>();
            Server server = FindObjectOfType<Server>();
            if (!server)
            {
                Server s = Instantiate(serverPrefab).GetComponent<Server>();
                s.Init();
            }
            c.clientName = PlayerName;//nameInput.text;
            c.isHost = true;
            if (c.clientName == "")
                c.clientName = "Host";
            c.ConnectToserver("127.0.0.1", 6321);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        mainMenu.SetActive(false);
        serverMenu.SetActive(true);
    }

    public void ConnectToServerButton()
    {
        string hostAddress = GameObject.Find("HostInput").GetComponent<InputField>().text;
        if (hostAddress == "")
            hostAddress = "127.0.0.1";

        try
        {
            Client c = Instantiate(clientPrefab).GetComponent<Client>();
            c.clientName = PlayerName;//nameInput.text;
            if (c.clientName == "")
                c.clientName = "Client";
            //c.isHost = false;
            c.ConnectToserver(hostAddress, 6321);
            connectMenu.SetActive(false);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    public void OkButtonCLick()
    {
        if (new User(loginInput.text, passwordInput.text).CheckUser())
        {
            PlayerName = loginInput.text;
            Login.SetActive(false);
            mainMenu.SetActive(true);
            Rating.text = test.GetRating(PlayerName);
            //Debug.Log("Click " + PlayerName);
        }
    }

    public void RegButtonClick()
    {
        Login.SetActive(false);
        Registration.SetActive(true);
    }

    public void BackButton()
    {
        mainMenu.SetActive(true);
        connectMenu.SetActive(false);
        serverMenu.SetActive(false);
        
        /*Server s = FindObjectOfType<Server>();
        if (s != null)
            Destroy(s.gameObject);*/

        Client c = FindObjectOfType<Client>();
        if (c != null)
            Destroy(c.gameObject);
    }

    private bool FullCheck()
    {
        if (loginInput_r.text == "" || passwordInput_r.text == "")
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void RegistrateButtonCLick()
    {
        if (FullCheck())
        {
            if (test.CheckLogin(loginInput_r.text, passwordInput_r.text))
            {
                test.NewUser(loginInput_r.text, passwordInput_r.text);
                RegBackButton();
            }
        }
    }

    public void RegBackButton()
    {
        Registration.SetActive(false);
        Login.SetActive(true);
    }

    public void ExitButtton()
    {
        //File.Delete("DB.db");
        Application.Quit();
    }
    public void StartGame()
    {
        /*
        SceneManager.MoveGameObjectToScene(connectMenu,  SceneManager.GetSceneByName("Login"));
        SceneManager.MoveGameObjectToScene(mainMenu, SceneManager.GetSceneByName("Login"));
        SceneManager.MoveGameObjectToScene(serverMenu, SceneManager.GetSceneByName("Login"));
        */
        SceneManager.LoadScene("Chess");
    }
}
