﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using Mono.Data.Sqlite;
using System.Data;
using System;
using UnityEngine.SceneManagement;

public class LoginManager : MonoBehaviour
{
    DataBase test = new DataBase();
    public static LoginManager Instace { set; get; }

    public GameObject mainMenu;
    public GameObject serverMenu;
    public GameObject connectMenu;
    public GameObject serverPrefab;
    public GameObject clientPrefab;
    //string PlayerName = Rules.Instance.GetName();
    //string PlayerName = GameManager.Instace.PlayerName;
    public Text Rating;
    public Text _PlayerName;

    private void Start()
    {
        Instace = this;
        test.DBCheck();
        connectMenu.SetActive(false);
        serverMenu.SetActive(false);
        mainMenu.SetActive(true);
        DontDestroyOnLoad(gameObject);
        string PlayerName = GameManager.Instace.PlayerName;
        _PlayerName.text = PlayerName;

        Rating.text = test.GetRating(PlayerName);
    }
    public void ConnectButton()
    {
        mainMenu.SetActive(false);
        connectMenu.SetActive(true);
    }
    //Client client = FindObjectOfType<Client>();
    public void HostButton()
    {
        Debug.Log("_PlayerName.text " + _PlayerName.text);
        try
        {
            //Debug.Log("_PlayerName.text " + _PlayerName.text);
            Client c = Instantiate(clientPrefab).GetComponent<Client>();
            Server server = FindObjectOfType<Server>();
            if (!server)
            {
                Server s = Instantiate(serverPrefab).GetComponent<Server>();
                s.Init();
            }
            c.clientName = _PlayerName.text;//nameInput.text;
            c.isHost = true;
            if (c.clientName == "")
                c.clientName = "Host";
            c.ConnectToserver("127.0.0.1", 6321);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        mainMenu.SetActive(false);
        serverMenu.SetActive(true);
    }

    public void ConnectToServerButton()
    {
        string hostAddress = GameObject.Find("HostInput").GetComponent<InputField>().text;
        if (hostAddress == "")
            hostAddress = "127.0.0.1";

        try
        {
            Client c = Instantiate(clientPrefab).GetComponent<Client>();
            c.clientName = _PlayerName.text;//nameInput.text;
            if (c.clientName == "")
                c.clientName = "Client";
            //c.isHost = false;
            c.ConnectToserver(hostAddress, 6321);
            connectMenu.SetActive(false);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }
   
    public void StartGame()
    {
        /*
        SceneManager.MoveGameObjectToScene(connectMenu,  SceneManager.GetSceneByName("Login"));
        SceneManager.MoveGameObjectToScene(mainMenu, SceneManager.GetSceneByName("Login"));
        SceneManager.MoveGameObjectToScene(serverMenu, SceneManager.GetSceneByName("Login"));
        */
        SceneManager.LoadScene("Chess");
    }
}
