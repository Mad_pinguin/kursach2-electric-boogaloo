﻿using System.Collections;
using System;
using System.Net;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.IO;

public class Client : MonoBehaviour
{
    public string clientName;
    private bool socketReady;
    private TcpClient socket;
    private NetworkStream stream;
    private StreamWriter writer;
    private StreamReader reader;
    public bool isHost=false;

    private List<GameCLient> players = new List<GameCLient>();

    private void Start()
    {
        DontDestroyOnLoad(gameObject); 
    }

    public bool ConnectToserver(string host, int port)
    {
        if (socketReady)
            return false;
        try
        {
            socket = new TcpClient(host, port);
            stream = socket.GetStream();
            writer = new StreamWriter(stream);
            reader = new StreamReader(stream);

            socketReady = true;
        }
        catch (Exception e)
        {
            Debug.Log("Socket error " + e.Message);
        }

        return socketReady;
    }

    private void Update()
    {
        if (socketReady)
        {
            if (stream.DataAvailable)
            {
                string data = reader.ReadLine();
                if (data != null)
                {
                    OnIncomingData(data);
                }
            }
        }
    }

    // Sending messages to the server
    public void Send(string data)
    {
        if (!socketReady)
            return;

        writer.WriteLine(data);
        writer.Flush();
    }
    
    // Read messages from the server
    private void OnIncomingData(string data)
    {
        //Debug.Log("Client:" + data);
        string[] aData = data.Split('|');

        switch (aData[0])
        {
            case "SWHO":
                for (int i = 1; i < aData.Length-1; i++)
                {
                    UserConnected(aData[i], false);
                }
                Send("CWHO|" + clientName + "|" + ((isHost)?1:0).ToString());
                break;
            case "SCNN":
                /*if (players.Count == 2)
                    break;*/
                UserConnected(aData[1], false);
                break;
            case "SMOV":
                string lastMove = aData[4];
                Rules.Instance.make_fen(lastMove);
                if (aData[1] == "Победили белые" || aData[1] == "Победили чёрные")
                {
                    Rules.Instance.winTime = Time.time;
                    Rules.Instance.winnerText.text = aData[1];
                    Rules.Instance.gameIsOver = true;
                }
                Rules.Instance.Update();
                if (aData[3] == "1" && (aData[1][0] == 'P' || aData[1][0] == 'R' || aData[1][0] == 'N' || aData[1][0] == 'B' || aData[1][0] == 'Q' || aData[1][0] == 'K'))
                {
                    Rules.Instance.MakeMove(aData[1]);
                }
                else
                    Rules.Instance.MakeMove("pa1a9");
                if (aData[3] == "0" && (aData[1][0] == 'p' || aData[1][0] == 'r' || aData[1][0] == 'n' || aData[1][0] == 'b' || aData[1][0] == 'q' || aData[1][0] == 'k'))
                    Rules.Instance.MakeMove(aData[1]);
                else
                    Rules.Instance.MakeMove("pa1a9");
                break;
        }
    }

    private void UserConnected(string name, bool host)
    {
        GameCLient c = new GameCLient();
        c.name = name;
        players.Add(c);
        //Debug.Log(players.Count);
        if (players.Count == 2)
        {
            GameManager.Instace.StartGame();
        }
        
    }
    
    public void OnApplicationQuit()
    {
        CloseSocket();
    }

    private void OnDisable()
    {
        CloseSocket();
    }
    private void CloseSocket()
    {
        if (!socketReady)
            return;
        writer.Close();
        reader.Close();
        socket.Close();
        socketReady = false;
    }
}

public class GameCLient
{
    public string name;
    public bool isHost;
}