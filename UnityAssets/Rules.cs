﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Chess_kur;
using UnityEngine.UI;

using UnityEngine.SceneManagement;


public class Rules : MonoBehaviour
{
    public static Rules Instance { set; get; } //!!!
    Dragndrop dnd;
    Chess chess;
    public GameObject test1;
    public Text notificationText;
    public Text turnText;
    public Text winnerText;
   /* public CanvasGroup alertCanvas;
    private float lastAlert;
    private bool alertActive;*/
    public bool gameIsOver;

    private Client client; //= FindObjectOfType<Client>();
    public bool isWhite;
    void sendNotification(string text)
    {
        notificationText.text = text;
    }
    string promotion = "";
    public float winTime;
    public Rules()
    {
        dnd = new Dragndrop();
        //chess = new Chess();
    }

    public void B_queen()
    {
        string[] parts = chess.fen.Split();
        if (parts[1] == "b")
        {
            promotion = "q";
        }
        else
        {
            promotion = "Q";
        }
    }

    public void B_rook()
    {
        string[] parts = chess.fen.Split();
        if (parts[1] == "b")
        {
            promotion = "r";
        }
        else
        {
            promotion = "R";
        }
    }

    public void B_bishop()
    {
        string[] parts = chess.fen.Split();
        if (parts[1] == "b")
        {
            promotion = "b";
        }
        else
        {
            promotion = "B";
        }
    }

    public void B_knight()
    {
        string[] parts = chess.fen.Split();
        if (parts[1] == "b")
        {
            promotion = "n";
        }
        else
        {
            promotion = "N";
        }
    }

    void Start()
    {
        //Client c = FindObjectOfType<Client>();
       // if(chess == null)
        chess = new Chess();
        Instance = this; //!!!
       // Alert("The game begins");
        client = FindObjectOfType<Client>();
        isWhite = client.isHost;
        //Debug.Log("I'm host - " + isWhite);
        string msg = "CMOV|";
        //msg += move;
        msg += "pa1a9" + "|" + chess.fen;
        client.Send(msg);
        ShowFigures();
    }

    public void MakeMove(string move)
    //public void MakeMove(string move, string fen)
    {
        //ShowFigures();
            chess = chess.Move(move); //делается ход
            if (chess.IsStalemate() == true)
            {
                sendNotification("Draw");
                winnerText.text = "Ничья";
                gameIsOver = true;
            winTime = Time.time;
            }
            if (chess.IsCheck() == true)
            {
                sendNotification("Check!");
                if (chess.IsMate() == true)
                {
                    if (chess.fen.Split()[1] == "w")
                    {
                        winnerText.text = "Победили чёрные";
                    }
                    else
                    {
                        winnerText.text = "Победили белые";
                    }
                    gameIsOver = true;
                winTime = Time.time;
                sendNotification("Mate!");
                }
            }
            ShowFigures();
            promotion = "";
            if (chess.fen.Split()[1] == "w")
            {
                turnText.text = "Ходят белые";
            }
            if (chess.fen.Split()[1] == "b")
            {
                turnText.text = "Ходят чёрные";
            }
            //Debug.Log(chess.fen);
    }

    public string get_fen()
    {
        return chess.fen;
        
    }

    public string PlayerName;
    public void make_fen(string fen)
    {
        chess = new Chess(fen);
        /*string msg = "CMOV|";
        //msg += move;
        msg += "pa1a9" + "|" + chess.fen;
        client.Send(msg);*/
        ShowFigures();
        //chess.fen = fen;
    }

    public void TryMove()
    {
        if (dnd.Action())
        {
            test1.SetActive(false);
            sendNotification("");
            string from = Getcoord(dnd.pickPosition);
            string to = Getcoord(dnd.dropPosition);
            string figure = chess.GetFigureAt((int)(dnd.pickPosition.x / 2.0), (int)(dnd.pickPosition.y / 2.0)).ToString();
            if (figure == "P" && dnd.dropPosition.y >= 13 && dnd.dropPosition.y <= 15 && chess.fen.Split()[1] == "w" && dnd.dropPosition.x <= 15 && dnd.dropPosition.x >= 0)
            {
                to = from;
                StartCoroutine(Test()); //возможный конец хода в функции
            }
            if (figure == "p" && (int)(dnd.dropPosition.y / 2.0) == 0 && chess.fen.Split()[1] == "b" && dnd.dropPosition.x <= 15 && dnd.dropPosition.x >= 0)
            {
                to = from;
                StartCoroutine(Test()); //возможный конец хода в функции
            }
            string move = figure + from + to + promotion;
            //MakeMove(move); //test
            //клиент-сервер
            string msg = "CMOV|";
            //msg += move;
            msg += move + "|" + chess.fen;
            client.Send(msg);
        }
    }

    //обработка хода при превращении пешки через панельку
    IEnumerator Test()
    {
        test1.SetActive(true);
        yield return new WaitWhile(() => promotion == "");
        //Debug.Log(promotion);
        test1.SetActive(false);
        string from = Getcoord(dnd.pickPosition);
        string to = Getcoord(dnd.dropPosition);
        string figure = chess.GetFigureAt((int)(dnd.pickPosition.x / 2.0), (int)(dnd.pickPosition.y / 2.0)).ToString();
        string move = figure + from + to + promotion;
        //клиент-сервер
        string msg = "CMOV|";
        //msg += move;
        msg += move + "|" + chess.fen;
        client.Send(msg);
        /*chess = chess.Move(move); //делается ход
            if (chess.IsCheck() == true)
        {
            sendNotification("Check!");
            if (chess.IsMate() == true)
            {
                if (chess.fen.Split()[1] == "w")
                {
                    winnerText.text = "Победили чёрные";
                }
                else
                {
                    winnerText.text = "Победили белые";
                }
                sendNotification("Mate!");
                gameIsOver = true;
            }
        }
        //Debug.Log(promotion);
        ShowFigures();
        promotion = "";

        if (chess.fen.Split()[1] == "w")
        {
            turnText.text = "Ходят белые";
        }
        if (chess.fen.Split()[1] == "b")
        {
            turnText.text = "Ходят чёрные";
        }*/
    }

    
    //void Update()
    public void Update()
    {
        if (gameIsOver)
        {
            if (Time.time - winTime > 5.0f)
            {

                Client client = FindObjectOfType<Client>();
                DataBase test = new DataBase();
                if (winnerText.text == "Победили белые")
                {
                    if (isWhite == true)
                    {
                        test.IncRating(client.clientName);
                    }
                    else
                    {
                        test.DecRating(client.clientName);
                    }
                }
                if (winnerText.text == "Победили чёрные")
                {
                    if (isWhite == false)
                    {
                        test.IncRating(client.clientName);
                    }
                    else
                    {
                        test.DecRating(client.clientName);
                    }
                }
                Server server = FindObjectOfType<Server>();
                GameManager manager = FindObjectOfType<GameManager>();
                /*if (server)
                    Destroy(server.gameObject);*/
                if (client)
                    Destroy(client.gameObject);
                if(manager)
                    Destroy(manager.gameObject);
                SceneManager.LoadScene("Login");
            }
            return;
        }
        //UpdateAlert();
        TryMove();
        //ShowFigures();
    }

    string Getcoord (Vector2 position)
    {
        int x = Convert.ToInt32(position.x / 2.0);
        int y = Convert.ToInt32(position.y / 2.0);
        return ((char)('a' + x)).ToString() + (y + 1).ToString();
    }

    public void ConcedeButton()
    {
        gameIsOver = true;
        winTime = Time.time;
        if (client.isHost == true)
            winnerText.text = "Победили чёрные";
        if (client.isHost == false)
            winnerText.text = "Победили белые";

        string msg = "CMOV|";
        //msg += move;
        msg += winnerText.text + "|" + chess.fen;
        client.Send(msg);
    }

    void ShowFigures()
    {
        int nr = 0;
        for (int y = 0; y < 8; y++)
            for (int x = 0; x < 8; x++)
            {
                string figure = chess.GetFigureAt(x, y).ToString();
                if (figure == ".") continue;
                PlaceFigure("box" + nr,figure, x, y);
                nr++;
            }
        for (; nr < 32; nr++)
        {
            PlaceFigure("box" + nr,"q",  9, 9);
        }
    }

    void PlaceFigure(string box, string figure, int x, int y)
    {
        GameObject goBox = GameObject.Find(box);
        GameObject goFigure = GameObject.Find(figure);
        GameObject goSquare = GameObject.Find("" + y + x);

        var spriteFigure = goFigure.GetComponent<SpriteRenderer>();
        var spriteBox = goBox.GetComponent<SpriteRenderer>();
        spriteBox.sprite = spriteFigure.sprite;
        
        goBox.transform.position = goSquare.transform.position;
    }
    /*
    public void Alert(string text)
    {
        alertCanvas.GetComponentInChildren<Text>().text = text;
        lastAlert = Time.time;
        alertActive = true;
    }
    public void UpdateAlert()
    {
        if (alertActive)
        {
            if (Time.time - lastAlert > 1.5f)
            {
                alertCanvas.alpha = 1 - ((Time.time - lastAlert)-1.5f);
                if (Time.time - lastAlert > 2.5f)
                {
                    alertActive = false;
                }
            }
        }
    }*/
}

class Dragndrop
{
    enum State
    {
        none, 
        drag
    }

    public Vector2 pickPosition { get; private set; }
    public Vector2 dropPosition { get; private set; }

    State state;
    GameObject item;
    Vector2 offset;

    public Dragndrop()
    {
        state = State.none;
        item = null;
    }

    public bool Action()
    {
        switch (state)
        {
            case State.none:
                if (IsMouseButonPressed())
                     PickUp();
                break;

            case State.drag:
                 if (IsMouseButonPressed())
                      Drag();
                 else
                 {
                      Drop();
                      return true;
                 }
                break;
        }
        return false;
    }
    bool IsMouseButonPressed()
    {
        return Input.GetMouseButton(0);
    }

    void PickUp()
    {
        Vector2 clickPosition = GetClickPosition();
        Transform clickedItem = GetItemAt(clickPosition);
        if (clickedItem == null) return;
        pickPosition = clickedItem.position;
        item = clickedItem.gameObject;
        state = State.drag;
        offset = pickPosition - clickPosition;
    }

    Transform GetItemAt(Vector2 position)
    {
        RaycastHit2D[] figures = Physics2D.RaycastAll(position, position, 0.5f);
        if (figures.Length == 0)
        {
            return null;
        }
        return figures[0].transform;
    }

    Vector2 GetClickPosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    void Drag()
    {
        item.transform.position = GetClickPosition() + offset;
    }

    void Drop()
    {
        dropPosition = item.transform.position;
        state = State.none;
        item = null;
    }
}