﻿using System.Collections;
using System;
using System.Net;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.IO;

public class Server : MonoBehaviour
{
    public int port = 6321;

    private List<ServerCLient> clients;
    private List<ServerCLient> disconnectList;

    private TcpListener server;
    private bool serverStarted;

    public void Init()
    {
        DontDestroyOnLoad(gameObject);
        clients = new List<ServerCLient>();
        disconnectList = new List<ServerCLient>();

        try
        {
            server = new TcpListener(IPAddress.Any, port);
            server.Start();

            StartListening();
            serverStarted = true;
        }
        catch (Exception e)
        {
            Debug.Log("Socket error: " + e.Message);
        }
    }

    private void Update()
    {
        if (!serverStarted)
            return;

        foreach (ServerCLient c in clients)
        {
            // is still connected?
            if (!IsConnected(c.tcp))
            {
                c.tcp.Close();
                disconnectList.Add(c);
                continue;
            }
            else
            {
                NetworkStream s = c.tcp.GetStream();
                if (s.DataAvailable)
                {
                    StreamReader reader = new StreamReader(s, true);
                    string data = reader.ReadLine();

                    if (data != null)
                    {
                        OnIncomingData(c, data);
                    }
                }
            }
        }

        for (int i = 0; i < disconnectList.Count - 1; i++)
        {
            clients.Remove(disconnectList[i]);
            disconnectList.RemoveAt(i);
        }
    }

    private void StartListening()
    {
        server.BeginAcceptTcpClient(AcceptTcpClient, server);
    }

    private void AcceptTcpClient(IAsyncResult ar)
    {
        TcpListener listener = (TcpListener)ar.AsyncState;

        string allUsers = "";
        foreach (ServerCLient i in clients)
        {
            allUsers += i.clientName + '|';
        }

        ServerCLient sc = new ServerCLient(listener.EndAcceptTcpClient(ar));
        clients.Add(sc);

        StartListening();
        
        Broadcast("SWHO|" + allUsers, clients[clients.Count - 1]);
    }

    private bool IsConnected(TcpClient c)
    {
        try
        {
            if (c != null && c.Client != null && c.Client.Connected)
            {
                if (c.Client.Poll(0, SelectMode.SelectRead))
                    return !(c.Client.Receive(new byte[1], SocketFlags.Peek) == 0);

                return true;
            }
            else
            {
                return false;
            }
        }
        catch
        {
            return false;
        }
    }

    // SErver send
    private void Broadcast(string data, List<ServerCLient> cl)
    {
        foreach (ServerCLient sc in cl)
        {
            try
            {
                StreamWriter writer = new StreamWriter(sc.tcp.GetStream());
                writer.WriteLine(data);
                writer.Flush();
            }
            catch (Exception e)
            {
                Debug.Log("Write error : " + e.Message);
            }
        }
    }

    private void Broadcast(string data, ServerCLient c)
    {
        List<ServerCLient> sc = new List<ServerCLient> { c };
        Broadcast(data, sc);
    }

    // Server read
    private void OnIncomingData(ServerCLient c, string data)
    {
        //Debug.Log("Server:" + data);
        string[] aData = data.Split('|');

        switch (aData[0])
        {
            case "CWHO":
                c.clientName = aData[1];
                c.isHost = (aData[2] == "0") ? false : true;
                Broadcast("SCNN|" + c.clientName, clients);
                //Debug.Log("this is fen1 " + Rules.Instance.get_fen());
                break;

            case "CMOV":
                //data.Replace('C', 'S');
                data = data.Replace('C', 'S');
                //Debug.Log("this is fen2 " + Rules.Instance.get_fen());
                //Debug.Log("Client " + c.clientName + " moves");
                Broadcast(data + "|" + ((c.isHost) ? 1 : 0).ToString() +"|" + Rules.Instance.get_fen(), clients);
                break;
        }
    }
}

public class ServerCLient
{
    public bool isHost;
    public string clientName;
    public TcpClient tcp;

    public ServerCLient(TcpClient tcp)
    {
        this.tcp = tcp;
    }
}