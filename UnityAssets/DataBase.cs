﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using System.Text;
using System.IO;
using System.Security.Cryptography;

public class DataBase
{
    public SqliteConnection UsersDB;
    private static SqliteCommand command;
    public bool DBCheck()
    {
        if (!File.Exists("DB.db"))
        {
            SqliteConnection.CreateFile("DB.db");
            UsersDB = new SqliteConnection("Data Source = DB.db; Version=3");
            command = new SqliteCommand(UsersDB);
            UsersDB.Open();
            command.Connection = UsersDB;
            command.CommandText = "CREATE TABLE Users(" +
            "ID INTEGER      PRIMARY KEY AUTOINCREMENT," +
            "Name VARCHAR (50) NOT NULL," +
            "Password VARCHAR(50) NOT NULL," +
            "Rating   INT DEFAULT(1000)" +
            ");";
            command.ExecuteNonQuery();
            Debug.Log("DB was created");
        }
        else
        {
            //Debug.Log("DB already exists");
            UsersDB = new SqliteConnection("Data Source = DB.db; Version=3");
            UsersDB.Open();
        }
        return true;
    }
    public bool CheckLogin(string Name, string Pass)
    {
        SqliteCommand Param = UsersDB.CreateCommand();
        Param.CommandText = "select Name from Users where Name like @l";
        Param.Parameters.Add("@l", System.Data.DbType.String).Value = Name;
        Param.Parameters.Add("@p", System.Data.DbType.String).Value = Pass;
        SqliteDataReader SQL = Param.ExecuteReader();
        if (SQL.HasRows)
        {
            while (SQL.Read())
            {
                return false;
            }
        }
        return true;
    }
    public string Hash(string input)
    {
        using (SHA1Managed sha1 = new SHA1Managed())
        {
            var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
            var sb = new StringBuilder(hash.Length * 2);
            foreach (byte b in hash)
            {
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }
    }
    public void NewUser(string Name, string Pass)
    {
        SqliteCommand Param1 = UsersDB.CreateCommand();
        Param1.CommandText = "insert into Users(Name,Password) values(@l,@p)";
        Param1.Parameters.Add("@l", System.Data.DbType.String).Value = Name;
        Param1.Parameters.Add("@p", System.Data.DbType.String).Value = Hash(Pass);
        Param1.ExecuteNonQuery();
    }

    public string GetRating(string Name)
    {
        SqliteCommand Param = UsersDB.CreateCommand();
        Param.CommandText = "select Rating from Users where Name like  @l";
        Param.Parameters.Add("@l", System.Data.DbType.String).Value = Name;
        string rate = Param.ExecuteScalar().ToString();
        return rate;
    }
    public void DecRating(string Name)
    {
        DBCheck();
        SqliteCommand Param1 = UsersDB.CreateCommand();
        Param1.CommandText = "update Users set Rating = Rating-10 where Name like @l";
        Param1.Parameters.Add("@l", System.Data.DbType.String).Value = Name;
        Param1.ExecuteNonQuery();
    }
    public void IncRating(string Name)
    {
        DBCheck();
        SqliteCommand Param1 = UsersDB.CreateCommand();
        Param1.CommandText = "update Users set Rating = Rating+10 where Name like @l";
        Param1.Parameters.Add("@l", System.Data.DbType.String).Value = Name;
        Param1.ExecuteNonQuery();
    }
}
